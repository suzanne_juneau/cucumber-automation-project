package steps;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import static org.junit.Assert.fail;

import org.junit.Assert;

import io.cucumber.java.en.*;
import io.github.bonigarcia.wdm.WebDriverManager;




public class EbayHome_Steps {
	WebDriver driver;
	
	@Given("Je suis sur la page d-accueil Ebay")
	public void je_suis_sur_la_page_d_accueil_Ebay() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.ebay.ca/");
	}

	@When("Lorsque je clique sur le lien recherche avancee")
	public void lorsque_je_clique_sur_le_lien_recherche_avancee() {
		driver.findElement(By.id("gh-as-a")).click();
	}

	@Then("Je navigue vers la page de recherche avancee")
	public void je_navigue_vers_la_page_de_recherche_avancee() throws InterruptedException {
		// Confirmer qu'on est bien sur la page de recherche avancee
		String urlattendu="https://www.ebay.ca/sch/ebayadvsearch";
		String TitreAttendu = "eBay Search: Advanced Search";
		String urlObtenu = driver.getCurrentUrl();
		String TitreObtenu = driver.getTitle();
		if(!TitreObtenu.equalsIgnoreCase(TitreAttendu)) {
			fail("Le titre de la page n'est pas celui attendue");
		}
		Assert.assertEquals(urlattendu, urlObtenu);
		Thread.sleep(4000);
		driver.quit();
	}
	
	
	//Scenario qui verifie le nombre de pages trouvees selon notre recherche
	@When("Lorsque je recherche un laptop")
	public void lorsque_je_recherche_un_laptop() {
	    driver.findElement(By.id("gh-ac")).sendKeys("laptop");
	    driver.findElement(By.id("gh-btn")).click();
	}

	
	
	@Then("je valide si {int} resultats sont affiches")
	public void je_valide_si_resultats_sont_affiches(Integer int1) {
	    
	}
	
	
	
	
	

}
