package steps;

import io.cucumber.java.en.*;


public class Demo_Steps {

	@Given("J-ouvre la page le navigateur")
	public void j_ouvre_la_page_le_navigateur() {
	   System.out.println("Step no 1");
	}

	@Given("Je suis sur la page de LogIn")
	public void je_suis_sur_la_page_de_LogIn() {
		System.out.println("Step no 2");
	}

	@When("Lorsque je saisis l-identifiant")
	public void lorsque_je_saisis_l_identifiant() {
		System.out.println("Step no 3");
	}

	@When("Je saisis le mot de passe")
	public void je_saisis_le_mot_de_passe() {
		System.out.println("Step no 4");
	}

	@When("Je clique sur le bouton Login")
	public void je_clique_sur_le_bouton_Login() {
		System.out.println("Step no 5");
	}

	@Then("Je suis sur la page d-acceuil")
	public void je_suis_sur_la_page_d_acceuil() {
		System.out.println("Step no 6");
	}

	@Then("Je valide que le mot Marketplace est affiche")
	public void je_valide_que_le_mot_Marketplace_est_affiche() {
		System.out.println("Step no 7");
	}

	
}
