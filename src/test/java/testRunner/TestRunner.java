package testRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features= {"features"}, 	//chemin du dossier contenant les features
		glue = {"steps"},  			//chemin du dossier contenant les steps definitions, glue code
		dryRun = false, 			//cherche les features qui n'ont pas de glue code et les runner
		monochrome = true,      	//formatter l'affichage dans la console
		tags = "@sc2home",			//sélectionner les scénarios à exécuter
		//name = "connecter",		//chercher et exécuter les scénarios qui contiennent l'expression
		plugin = {"pretty","json:target/json-report/cucumber.json"}
		)
public class TestRunner {

}
