$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:features/ebayHome.feature");
formatter.feature({
  "name": "Les fonctionnalites de Ebay Homepage",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Scenario qui teste la recherche avancee",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@sc1"
    }
  ]
});
formatter.step({
  "name": "Je suis sur la page d-accueil Ebay",
  "keyword": "Given "
});
formatter.match({
  "location": "steps.EbayHome_Steps.je_suis_sur_la_page_d_accueil_Ebay()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Lorsque je clique sur le lien recherche avancee",
  "keyword": "When "
});
formatter.match({
  "location": "steps.EbayHome_Steps.lorsque_je_clique_sur_le_lien_recherche_avancee()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Je navigue vers la page de recherche avancee",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.EbayHome_Steps.je_navigue_vers_la_page_de_recherche_avancee()"
});
formatter.result({
  "status": "passed"
});
});