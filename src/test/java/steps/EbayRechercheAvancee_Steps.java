package steps;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.cucumber.java.en.*;
import io.github.bonigarcia.wdm.WebDriverManager;

public class EbayRechercheAvancee_Steps {
	WebDriver driver;
	
	@Given("Je suis sur la page de recherche avancee de Ebay")
	public void je_suis_sur_la_page_de_recherche_avancee_de_Ebay() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.ebay.ca/sch/ebayadvsearch");
	}

	@When("Lorsque je clique sur le logo Ebay")
	public void lorsque_je_clique_sur_le_logo_Ebay() {
		driver.findElement(By.id("gh-la")).click();
	}

	@Then("Je navigue vers la page d-accueil Ebay")
	public void je_navigue_vers_la_page_d_accueil_Ebay() {
		String TitreObt = driver.getTitle();
		String UrlObt = driver.getCurrentUrl();
		String TitreAtt = "Electronics, Cars, Fashion, Collectibles & More | eBay";
		String UrlAtt = "https://www.ebay.ca/";
		if(! TitreObt.equalsIgnoreCase(TitreAtt)) {
		fail("La page est differente de la page attendu");
		}
		Assert.assertEquals(UrlAtt, UrlObt);
		driver.quit();
	}

	

}
