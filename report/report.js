$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:features/demo.feature");
formatter.feature({
  "name": "Application Orange",
  "description": "  Cette fonctionnalite permet de se connecter a l-application Orange",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@essai"
    }
  ]
});
formatter.scenario({
  "name": "Se connecter a l-application Orange avec un identifiant valide",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@essai"
    },
    {
      "name": "@essai1"
    }
  ]
});
formatter.step({
  "name": "J-ouvre la page le navigateur",
  "keyword": "Given "
});
formatter.match({
  "location": "steps.Demo_Steps.j_ouvre_la_page_le_navigateur()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Je suis sur la page de LogIn",
  "keyword": "And "
});
formatter.match({
  "location": "steps.Demo_Steps.je_suis_sur_la_page_de_LogIn()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Lorsque je saisis l-identifiant",
  "keyword": "When "
});
formatter.match({
  "location": "steps.Demo_Steps.lorsque_je_saisis_l_identifiant()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Je saisis le mot de passe",
  "keyword": "And "
});
formatter.match({
  "location": "steps.Demo_Steps.je_saisis_le_mot_de_passe()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Je clique sur le bouton Login",
  "keyword": "And "
});
formatter.match({
  "location": "steps.Demo_Steps.je_clique_sur_le_bouton_Login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Je suis sur la page d-acceuil",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.Demo_Steps.je_suis_sur_la_page_d_acceuil()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Je valide que le mot Marketplace est affiche",
  "keyword": "And "
});
formatter.match({
  "location": "steps.Demo_Steps.je_valide_que_le_mot_Marketplace_est_affiche()"
});
formatter.result({
  "status": "passed"
});
});